Srpring con anotaciones
======================
Aplicando SpringMVC con anotaciones, spring security, spring-xml
---------------------------------------------------------------
Este es un pequeño proyecto en su primera versión, el cual esta 
realizado con spring mvc aplicando el patrón inversión de control con la 
técnica inyección de dependencias con anotaciones.("las imagenes 
utilizadas de entidades privadas son creditos de sus respectivos 
creadores, esta aplicacion esta realizada solo con finalidad educativa").

La aplicacion tiene una seccion de productos la cual esta restringida, y solo
los usuarios con un rol de administrados pueden ingresar a ella, lo anterior mencionado
se realizo con spring-securty.

Base de Datos
=============
La bd contine 4 tablas sencillas para el funcionamiento de este ejemplo, 

credenciales para login
-------------------------
usuario: ramon   password: 123       tiene un rol de usuario
usuario: alex    password: alex123   tiene un rol de usuario y administrador 


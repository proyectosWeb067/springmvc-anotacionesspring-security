-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2019 a las 00:44:50
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `spring1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idproducto` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `precio` double(10,2) NOT NULL,
  `descripcion` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idproducto`, `titulo`, `tipo`, `precio`, `descripcion`) VALUES
(7, 'Seguro', 'empresa', 500.00, 'este producto, brinda a su empresa el respaldo que necesita en caso de riesgo.'),
(9, 'Targeta de credito', 'Empresa', 200.00, 'Con este producto bancolombiano, usted podrÃ¡ hacer sus sueÃ±os realidad pues los instereses son bajos y ademas usted podrÃ¡ acumular puntos por sus compras y asÃ­ ganas muchos premios. '),
(10, 'Cuenta de Ahorros', 'Personal', 200.00, 'Cuenta exclusiva para personas que trabajen en empresas que tengan convenio de pago de nÃ³mina con Bancolombiano.'),
(11, 'Fiducuenta', 'Personal', 300.00, 'Encuentra en la Fiducuenta una opciÃ³n de inversiÃ³n a corto plazo, manteniendo disponible tu dinero para el manejo de tu liquidez.'),
(12, 'Credito', 'Vivienda', 200.00, 'Adquiere, construye o reforma tu vivienda nueva o usada con el CrÃ©dito de Vivienda que tenemos para ti.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `user_role_id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`user_role_id`, `username`, `role`) VALUES
(2, 'alex', 'ROLE_ADMIN'),
(3, 'alex', 'ROLE_USER'),
(1, 'ramon', 'ROLE_USER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellpat` varchar(30) NOT NULL,
  `apellmat` varchar(30) NOT NULL,
  `tipodoc` varchar(30) NOT NULL,
  `numdoc` int(11) NOT NULL,
  `dianac` int(11) NOT NULL,
  `mesnac` varchar(30) DEFAULT NULL,
  `anac` int(11) NOT NULL,
  `sexo` varchar(15) NOT NULL,
  `ruc` int(11) NOT NULL,
  `estadcivil` varchar(30) NOT NULL,
  `estadolabo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellpat`, `apellmat`, `tipodoc`, `numdoc`, `dianac`, `mesnac`, `anac`, `sexo`, `ruc`, `estadcivil`, `estadolabo`) VALUES
(1, 'ramon', 'valdez', 'garcilazo', 'cedula', 12123123, 20, 'agosto', 1934, 'masculino', 433442123, 'casado', 'desempleado'),
(2, 'maria', 'beatriz', 'echandia', 'cedula', 3456432, 2, 'enero', 1967, 'femenino', 34234432, 'soltera', 'trabajando'),
(3, 'helena', 'gutierrez', 'giraldo', 'C.C', 28763435, 1, 'Enero', 1900, '0', 123123, 'Soltero', 'Trabajando'),
(4, 'alejandra ', 'salazar', 'pombo', 'C.C', 25789098, 1, 'Enero', 1900, '0', 987654, 'Divorsiado', 'Trabajando'),
(5, 'catalina ', 'velez', 'buitrago', 'C.C', 30008123, 1, 'Febrero', 1970, 'femenino', 934354, 'Divorsiado', 'Trabajando'),
(6, 'Raquel hercole', 'velez', 'buitrago', 'C.C', 30008123, 5, 'Octubre', 1980, 'femenino', 934354, 'Casada', 'Trabajando'),
(7, 'carol', 'perez', 'tobon', 'C.C', 4099832, 5, 'Octubre', 1984, 'femenino', 0, 'Casada', 'Trabajando'),
(12, 'claudia', 'valdez', 'aristizabal', 'cedula', 32123452, 20, 'agosto', 1954, 'femnino', 433442123, 'casado', 'desempleado'),
(13, 'karol', 'henriquez', 'zaabedra', 'cedula', 1081236432, 20, 'agosto', 1984, 'femnino', 433442123, 'casado', 'desempleado'),
(14, 'natalia ', 'paris', 'giraldo', 'C.C', 34234432, 1, 'Enero', 1978, 'Femenino', 800987632, 'Casado', 'Trabajando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariologin`
--

CREATE TABLE `usuariologin` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuariologin`
--

INSERT INTO `usuariologin` (`username`, `password`, `enabled`) VALUES
('alex', 'alex123', 1),
('ramon', '123', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproducto`),
  ADD UNIQUE KEY `IDX_PRODUCTOS_1` (`titulo`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`user_role_id`),
  ADD UNIQUE KEY `uni_username_role` (`role`,`username`),
  ADD KEY `fk_username_idx` (`username`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indices de la tabla `usuariologin`
--
ALTER TABLE `usuariologin`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `rol`
--
ALTER TABLE `rol`
  ADD CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `usuariologin` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css"/>"  media="screen,projection"/>
        <title>Productos</title>
    </head>
    <body onload="del();">

     
            
            <nav>
                <div class="nav-wrapper blue">
                    <a href="#"  class="brand-logo"><img src="<c:url value="/img/bcolombia.jpg"/>" style="max-height: 80px; max-width: 100px;"/></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="../app/Inicio">Inicio</a></li>
                        <li><a href="../app/accion=In">Agregar Producto</a></li>
                        <li><a href="#">Editar Producto</a></li>

                    </ul>
                </div>
            </nav>

            <main class="container"> 

                <div class="row">
                    <c:url value="/j_spring_security_logout" var="logoutUrl" />
                    <form action="${logoutUrl}" method="post" id="logoutForm">
                        <input type="hidden" name="${_csrf.parameterName}"
                               value="${_csrf.token}" />
                    </form>
                    <c:if test="${pageContext.request.userPrincipal.name != null}">
                        <h2 style="font-size: 20px;">
                                Bienvenido : ${pageContext.request.userPrincipal.name} | <a
                                        href="javascript:formSubmit()">Cerrar Sesión</a>
                        </h2>
                    </c:if>
                    <h3>Productos bancolombiano</h3>
                  

                    <table class="infoDel">
                        <thead>
                            <tr>
                                <td>Producto </td>
                                <td>Tipo</td>
                                <td>Precio</td>
                                <td class=" blue-text"><a class="elimi" href="#">Eliminar</a></td>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach var="p" items="${listapro}">
                                <tr>
                                    <td>${p.titulo}</td>
                                    <td>${p.tipo}</td>
                                    <td>${p.precio}</td>  
                                    <td>
                                        <label for="${p.idproducto}">
                                            <input  type="checkbox" name="del" id="${p.idproducto}" value="${p.idproducto}" />

                                            <span> </span>
                                        </label>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <a href="../app/Inicio">Regresar</a>
                </div>   
            </main> 

            <footer class="page-footer background blue">
                <div class="container">
                    <div class="row">
                        <div class="col l6 s12">
                            <h5 class="white-text">Descripción.</h5>
                            <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                                aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                        </div>
                        <div class="col l4 offset-l2 s12">
                            <h5 class="white-text">Links</h5>
                            <ul>
                                <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        © 2019 Copyright Text
                        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                    </div>
                </div>
            </footer>    

            <script type="text/javascript" src="<c:url value="https://code.jquery.com/jquery-3.2.1.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/js/materialize.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/js/del.js"/>"></script>
            <script type="text/javascript">
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
            </script>
         
    </body>
</html>

 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css"/>"  media="screen,projection"/>

        <style>body {display: flex;min-height: 100vh;flex-direction: column;}main {flex: 1 0 auto;}</style>
        <title>Targeta de Credito</title>

    </head>
    <body>
        <!-- <h1>Area de formulario !</h1>
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
         <h3 style="text-align: center;"> 
             --------------------------------------------------------------------           
             Solicitud de targeta 
     
             --------------------------------------------------------------------  
         </h3>
        
        https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js
        
         $(document).ready(function () {
                $('select').material_select();
            });
        -->

        <nav>
            <div class="nav-wrapper background blue">
                <form>
                    <div class="input-field">
                        <a href="#"  class="brand-logo"><img src="<c:url value="/img/bcolombia.jpg"/>" style="max-height: 80px; max-width: 100px;"/></a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            <li><a href="../app/Inicio">Inicio</a></li>

                        </ul>
                    </div>
                </form>
            </div>
        </nav>

        <main class="container">
            <div class="row">
                <f:form modelAttribute="persona" action="../app/accion=insok" method="POST">

                    <div class="row">
                        <div class="input-field col s4">
                            <f:label  path="nombre" >Nombre</f:label>   
                            <f:input  path="nombre" />   
                        </div>

                        <div class="input-field col s3">
                            <f:label  path="apellPat" >Apellido Paterno</f:label>   
                            <f:input  path="apellPat"  />   
                        </div>

                        <div class="input-field col s3" >
                            <f:label  path="apellMat" >Apellido Materno</f:label>   
                            <f:input  path="apellMat"  />   
                        </div>
                        
                        <div class="input-field col s3">    
                            <f:label  path="ruc"  >Ruc</f:label>   
                            <f:input  path="ruc" /> 
                            
                        </div>                     
                    </div>


                    <div class="row col s12">   
                        <label class="col s3" style="font-size: 15px;">Fecha Nacimiento: </label>
                        <div class="input-field col s3">   
                            <f:select path="dianac" items="${ld}"/>
                                <label>Dia</label>    
                        </div>

                        <div class="input-field col s3"> 
                             <f:select path="mesnac" items="${lm}"/>
                                 <label>Mes</label>
                        </div>
                        
                        <div class="input-field col s3">   
                            <f:select path="anac" items="${la}"/>
                           <label>Año</label>
                        </div>
                         
                    </div> 


                    <div class="row col s12">
                        <label class="col s2"style="font-size: 15px;">Tipo Documento: </label>
                        <div class=" col s3"> 
                           <label> 
                            <f:radiobutton path="tipodoc" value="cc" id="cc" />
                            <span>Cedula</span>
                           </label> 
                        </div>
                       <div class=" col s3"> 
                           <label>
                            <f:radiobutton path="tipodoc" value="ce" id="ce" />
                             <span>Cedula Extrangeria</span>
                            </label> 
                        </div> 
                        
                        <div class="input-field col s3">                             
                            <f:label  path="numdoc" >Numero Documento</f:label>   
                            <f:input  path="numdoc"  />   
                        </div >
                        
                    </div> 
                        
                    <div class="row col s12">
                        <label class="col s2" style="font-size:  15px;">Sexo: </label>
                        <div class="col s3">
                            <label>
                                <f:radiobutton path="sexo" value="m" id="m" /> 
                                <span>Masculino</span>
                            </label>
                            
                        </div>
                         <div class="col s2">
                             <label>
                                 <f:radiobutton path="sexo" value="f" id="f" />
                                 <span>Femenino</span>
                             </label> 
                             
                        </div>
                    </div>  
                    
                    <div class="row col s12">
                        <label class="col s2" style="font-size: 15px;">Estado Civil: </label>
                        <div class="col s3">
                          <label>
                             <f:radiobutton path="estadcivil" value="c" id="c" />
                             <span>Soltero(a)</span> 
                          </label>
                        </div>  
                        <div class="col s3">
                           <label> 
                            <f:radiobutton path="estadcivil" value="c" id="c" /> 
                            <span>Casado(a)</span> 
                           </label> 
                        </div>
                        <div class="col s3">
                            <label> 
                            <f:radiobutton path="estadcivil" value="d" id="d" />
                            <span>Divorciado(a)</span
                             </label> 
                        </div>
                    </div> 
                        
                        
                    <div class="row col s12">
                        <label class="col s2" style="font-size: 15px;">Situación Laboral:</label>
                        <div class="col s3">
                            <label>
                                <f:radiobutton path="estadolabo" value="t" id="t" />
                                <span>Trabajando</span> 
                            </label>
                            
                        </div>
                        <div class="col s4">
                            <label>
                                <f:radiobutton path="estadolabo" value="de" id="de" />
                                <span>Desempleado</span>
                            </label>
                               
                        </div>
                            
                    </div>  
                    <div class="row col s12">
                        <input class="waves-effect waves-light btn" type="submit" value="Enviar"/>  
                    </div>   
                   
                </f:form> 
            </div>   
        </main>

        <footer class="page-footer background blue">

            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Descripción.</h5>
                        <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                            aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2019 Copyright Text
                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                </div>
            </div>
        </footer> 


        <script type="text/javascript" src="<c:url value=" https://code.jquery.com/jquery-3.2.1.min.js"/>"></script>               
        <script type="text/javascript" src="<c:url value="/js/materialize.min.js"/>"></script> 
        <!--script type="text/javascript" src="<  value="/js/materialize.min.js"/>"/
        https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js
        https://code.jquery.com/jquery-3.2.1.min.js
        
        
        https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css
        https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js
        https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.js
        -->               

        <script type="text/javascript">
            // Or with jQuery

            $(document).ready(function () {
                $('select').formSelect();

            });

        </script>

    </body>
</html>




<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css" />"  media="screen,projection"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/estilonav.css"/>" media="screen" />

        <title>Proyecto Bancolombiano</title>
    </head>

    <body>
        <sec:authorize access="hasRole('ROLE_USER')"> 
            <header
                <nav>
                    <img src="<c:url value="/img/bcolombia.jpg" />"/>  
                    <div class="contenedor">
                        <tr>
                            <td> <a href="../app/accion=nos-li">Nosotros </a> </td>&nbsp|&nbsp 
                            <td> <a href="#">Servicio al cliente </a> </td>&nbsp|&nbsp 
                            <td> <a href="../app/p01">Targeta Credito </a> </td>&nbsp|&nbsp 
                            <td> <a href="../app/accion=lista">Productos</a> </td>&nbsp|&nbsp
                            <td> <a href="#">Mapa de Sitio</a> </td>
                        </tr>
                        <tr >
                            <c:if test="${pageContext.request.userPrincipal.name != null}">
                                <h1 style="font-size: 20px;">
                                    Bienvenido: ${pageContext.request.userPrincipal.name} | <a
                                        href="javascript:formSubmit()"> Cerrar Sesión</a>
                                </h1>
                            </c:if> 
                        </tr>
                    </div>  
                </nav>
            </header>
            <section class="container">
                <div> 
                    <div class="contenido">
                        <h1>Bienvenido, proyecto bancolombiano 1.0</h1>
                        <div> 

                            <p>Este es un proyecto realizado con fines  educativos y además con el proposito de 
                                poner en practica algunos conceptos del framework spring con el cual se pueden desarrollar aplicativos web
                                escalables y con una mayor rapides. las tecnologias que fueron implementadas para el desarrollo
                                de este pequeño ejemplo funcional son: spring framework version 4, materialize, una pequeña base de datos
                                la cual solo esta para persistir y servir los datos.
                            </p>
                            <p>
                                En esta primera version se pretende poner en practica el diseño modelo, controlador, vista, servicio
                                aplicando el patron Inversion de control con la tecnica de inyeccion de dependencias; esta version 
                                esta configurada con xml, es decir se presenta al archivo applicationContext.xml las inyecciones de dependencia
                                y respectivamente al archivo dispatcher-servlet.xml los controladores.</p>


                        </div>
                        <p>
                        <h4>Usuarios que han solicitado targeta <a href="../app/acc=getu" style="text-decoration: underline;">ver</a></h4>
                        </p>

                    </div> 
                </div>  
                <!-- For login user -->
                <c:url value="/j_spring_security_logout" var="logoutUrl" />
                <form action="${logoutUrl}" method="post" id="logoutForm">
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}" />
                </form>

            </section>   
            <footer class="page-footer background blue">
                <div class="container">
                    <div class="row">
                        <div class="col l6 s12">
                            <h5 class="white-text">Descripción.</h5>
                            <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                                aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                        </div>
                        <div class="col l4 offset-l2 s12">
                            <h5 class="white-text">Links</h5>
                            <ul>
                                <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        © 2019 Copyright Text
                        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                    </div>
                </div>
            </footer>    

        </sec:authorize>   
        <script type="text/javascript">
            function formSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
    </body>
</html>

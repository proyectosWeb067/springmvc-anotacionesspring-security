<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css" />"  media="screen,projection"/>

        <title>Nosotros</title>

    </head>
    <body>
        <nav>
            <div class="nav-wrapper background blue">
                <a href="#" class="brand-logo"><img src="<c:url value="/img/bcolombia.jpg"/>" style="max-height: 80px; max-width: 100px;"/></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="../app/Inicio">Inicio</a></li>

                </ul>
            </div>
        </nav>
        <section class="container">

            <div class="row">
                <h5>Nuestros Productos</h5>
                <c:forEach items="${li}"  var="pro" >

                    <div class="col s12 m6" style="float: left;">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">${pro.titulo}</span>
                                <p>${pro.descripcion}</p>
                            </div>
                        </div> 
                    </div>

                </c:forEach>


                <div class="carousel">
                    <h5>Por su fidelidad con nosotros presentamos algunos destinos a los que puede viajar</h5>
                    <a class="carousel-item" href="#one!"><img src="https://lorempixel.com/250/250/nature/1"></a>
                    <a class="carousel-item" href="#two!"><img src="https://lorempixel.com/250/250/nature/2"></a>
                    <a class="carousel-item" href="#three!"><img src="https://lorempixel.com/250/250/nature/3"></a>
                    <a class="carousel-item" href="#four!"><img src="https://lorempixel.com/250/250/nature/4"></a>
                    <a class="carousel-item" href="#five!"><img src="https://lorempixel.com/250/250/nature/5"></a>
                </div>
            </div>
        </section>    



        <footer class="page-footer background blue" >
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Descripción.</h5>
                        <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                            aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2019 Copyright Text
                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                </div>
            </div>
        </footer>
        <script type="text/javascript" src="<c:url value="https://code.jquery.com/jquery-3.2.1.min.js"/>"></script>

        <script type="text/javascript" src="<c:url value="/js/materialize.js"/>"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.carousel').carousel();
            });
        </script>
    </body>
</html>

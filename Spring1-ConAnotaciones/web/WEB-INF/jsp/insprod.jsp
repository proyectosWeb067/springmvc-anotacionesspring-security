
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css"/>"  media="screen,projection"/>
        <title>Nuevo Producto</title>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper background blue">
                <form>
                    <div class="input-field">
                        <a href="#"  class="brand-logo"><img src="<c:url value="/img/bcolombia.jpg"/>" style="max-height: 80px; max-width: 100px;"/></a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            <li><a href="../app/Inicio">Inicio</a></li>

                        </ul>
                    </div>
                </form>
            </div>
        </nav>
        <main class="container">

            <div class="row">     
                <h4>Estas agregando un nuevo producto...</h4>
                <div class="col m5">
                    <f:form modelAttribute="per" action="../app/accion=Inok" method="POST">

                        <tr> 
                            <td>Producto:</td>
                            <td ><f:input path="titulo" /></td>
                        </tr>  <br/><br/>
                        <tr> 
                            <td>Tipo:</td> 
                            <td  ><f:input path="tipo"/></td> 
                        </tr> <br/><br/>
                        <tr> 
                            <td>Precio:</td>  
                            <td><f:input path="precio"/></td>
                        </tr> <br/><br/>
                        <tr> 
                            <td>Descripción:</td>  
                            <td><f:textarea path="descripcion" rows="5" cols="30"/> </td>
                        </tr> <br/><br/>
                        <tr>
                        <input type="submit" name="Enviar"/>
                        </tr> 
                    </f:form>
                </div>     
            </div>     
        </main>
        <footer class="page-footer background blue">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Descripción.</h5>
                        <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                            aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2019 Copyright Text
                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                </div>
            </div>
        </footer>      


    </body>
</html>

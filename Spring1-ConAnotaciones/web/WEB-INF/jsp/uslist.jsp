<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css" />"  media="screen,projection"/>

        <title>Solicitud</title>
    </head>
    <body>

        <nav>
            <div class="nav-wrapper blue">
                <a href="#" class="brand-logo"><img src="<c:url value="/img/bcolombia.jpg"/>" style="max-height: 80px; max-width: 100px;"/></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="../app/Inicio">Inicio</a></li>

                </ul>
            </div>
        </nav>
        <section class="container"> 
            <div class="row">  
                <h3>Usuarios con solicitud realizada</h3>
                <table class="striped">
                    <thead>
                    <td class="blue-text text-darken-2">Nombre</td>
                    <td class="blue-text text-darken-2">Apellidos</td>
                    <td class="blue-text text-darken-2">Situacion Laboral</td>
                    <td class="blue-text text-darken-2">Numero documento</td>
                    </thead>
                    <tbody>
                        <c:forEach items="${alluser}" var="u">
                            <tr>
                                <td> ${u.nombre}</td>
                                <td> ${u.apellPat} ${u.apellMat}</td>
                                <td>${u.estadolabo}</td>
                                <td>${u.numdoc}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>
            <a href="index.htm" class="text-darken-2">Regresar</a>

        </section> 
        <footer class="page-footer background blue">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Descripción.</h5>
                        <p class="grey-text text-lighten-4">Este es un proyecto, con fines educativos, 
                            aplicando los conceptos del frameork spring MVC, con el framework materialize.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Bancolombiano</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2019 Copyright Text
                    <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
                </div>
            </div>
        </footer>   
    </body>
</html>

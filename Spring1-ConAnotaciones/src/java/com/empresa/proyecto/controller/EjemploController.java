
package com.empresa.proyecto.controller;

import com.empresa.proyecto.dto.PersonaDTO;
import com.empresa.proyecto.dto.ProductoDTO;
import com.empresa.proyecto.service.ProductoService;
import com.empresa.proyecto.service.UsuarioService; 
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestParam;
 
/**
 *
 * @author yio
 */


//@RequestMapping("app") puedo colocar aqui para que ingrese al vista por deecto

@RequestMapping(value = "app")
@Controller
public class EjemploController {
    
    @Autowired //inyeccion de dependencias con anotacion autowired
    ProductoService pservice;
    @Autowired 
    UsuarioService uservice;
    
    //----------------------------------METODO POR DEFECTO----------------------------------------------
    
    //cuando ingrese al controlador se abrira la vista de este metodo(index)
     @RequestMapping(value="Inicio",  method = RequestMethod.GET)
      public ModelAndView vistaDefecto(){
        ModelAndView mav = new ModelAndView("index");
        return mav;
     } 
     //---------------------------------C R U D - PRODUCTOS ---------------------------------------------
    @RequestMapping(value="accion=lista", method = RequestMethod.GET)
    public ModelAndView listaProductos() {
        ModelAndView mav = grilla();
        return mav;
    }
    public ModelAndView grilla() {  
        List<ProductoDTO> lista = pservice.getAll();
        ModelAndView mav = new ModelAndView("productos"); 
        mav.addObject("listapro", lista);
      
        return mav;
    }
    
     //---- lista productos en pagina Nostros---
    @RequestMapping(value="accion=nos-li", method = RequestMethod.GET )
    public ModelAndView listProdu(){
        ModelAndView mav = datosNosotros();
        return mav;
    }
    
   
    public ModelAndView datosNosotros(){
         List<ProductoDTO> l = pservice.getAll();
        ModelAndView mav = new ModelAndView("nosotros");
        mav.addObject("li", l);
        return mav;
    }
    //------------------------------------------------
    
    
    //-------------------insertando producto---------------------
    @RequestMapping(value="accion=In", method=RequestMethod.GET)
    public ModelAndView insertProducto(){
        ModelAndView mav = new ModelAndView("insprod");
        
        ProductoDTO p = new ProductoDTO();
        mav.addObject("per", p);
    
        return mav;
    }
    
    @RequestMapping(value = "accion=Inok", method= RequestMethod.POST)
    public ModelAndView insertInfoProducto(@ModelAttribute ProductoDTO per){
        ModelAndView mav = new ModelAndView("salidaformulario");
        String r = pservice.insertarProducto(per);
        mav.addObject("info", r);
        
          return mav;
    }
 //-----------------------------------------------------------------------
    
  //-----------------------eliminando productos-----------------------------
  
 
  @RequestMapping(value = "accion=del", method = RequestMethod.GET)
  public ModelAndView deleteProducto(HttpServletRequest request ){
      String ids = request.getParameter("ids");  
      ModelAndView mav = new ModelAndView("salidaformulario");
     
      List<Object[]> pids = new ArrayList();
      if (ids != null) {
          String[] idsOn = ids.split(",");
          
          for(String id : idsOn){
              pids.add(new Object[]{Integer.valueOf(id)});
              System.out.println("---->" +id);
          }
          
          String resp = pservice.eliminarProducto(pids);
           mav.addObject("meninfodelpro", resp);
      }
     
      return mav;
  }
  
   
 //----------------------------------------------------------------------------   
    
  //-------------------------------------------------------------------------
    
    
  //--------------------------------------------------------------------------------------
    
  //------------------------------------ C R U D - USUARIO -------------------------------
    
    @RequestMapping(value="acc=getu", method = RequestMethod.GET )
    public ModelAndView listuser(){
        ModelAndView mav = getUsers();
        return mav;
    }
  
  
    public ModelAndView getUsers(){
        ModelAndView mav = new ModelAndView("uslist");
        List<PersonaDTO> l = uservice.getAll();
        mav.addObject("alluser",l);
        return mav;    
    }
  //----------------------------------------------------------------------------------
  //-------------------------------------INSERTANDO CLIENTE--------------------------- 
    //en el navegador ---->  app/p01
    //Entrada de datos del controlador al jsp
    @RequestMapping(value="p01", method=RequestMethod.GET)
    public ModelAndView Formulario1() {
        ModelAndView mv = new ModelAndView("formulario"); //pagina jsp
        PersonaDTO p = new PersonaDTO();
       
        List<Integer> dia = new ArrayList<>();

        for (int i = 1; i <= 31; i++) {
            dia.add(i);
        }
        List<String> mes = new ArrayList<>();
        mes.add("Enero");
        mes.add("Febrero");
        mes.add("Marzo");
        mes.add("Abril");
        mes.add("Mayo");
        mes.add("Junio");
        mes.add("Julio");
        mes.add("Agosto");
        mes.add("Septiembre");
        mes.add("Octubre");
        mes.add("Noviembre");
        mes.add("Diciembre");

        List<Integer> a = new ArrayList<>();
        
       
        for (int i = 1920; i <= 2019; i++) {
            a.add(i);
        }
        //invirtiendo el orden de los años del 2019 al 1920
        Collections.sort(a);
        Comparator<Integer> comparador = Collections.reverseOrder();
        Collections.sort(a, comparador);
         Collections.reverseOrder();
         
        
        mv.addObject("ld", dia);
        mv.addObject("lm", mes);
        mv.addObject("la", a);
        mv.addObject("persona", p); // del controlador envio el objeto al jsp
        return mv;
    }
    
    @RequestMapping(value = "accion=insok", method=RequestMethod.POST)
    public ModelAndView insertUsuer(@ModelAttribute PersonaDTO persona){
        ModelAndView mav = new ModelAndView("salidaformulario");
       
        String eCiv = persona.getEstadcivil();
        String eLab = persona.getEstadolabo();
        switch (eCiv) {

           case "s":
               persona.setEstadcivil("Soltero");
                break;
            case "d":
               persona.setEstadcivil("Divorciado");
                break;
            case "c":
                persona.setEstadcivil("Casado");;
                break;

            default:
                persona.setEstadcivil("Sin Dato");
                break;

        }

        switch (eLab) {

            case "t":
                persona.setEstadolabo("Trabajando");
                break;

            case "de":
                persona.setEstadolabo("Desempleado");
                break;

            default:
                persona.setEstadolabo("Sin Dato");
                break;

        }

        if (persona.getSexo().equals("m")){
            persona.setSexo("Masculino");   
        }else{
            persona.setSexo("Femenino");
        }
        
        
        if (persona.getTipodoc().equals("cc")) {
            persona.setTipodoc("C.C");
        } else {
            persona.setTipodoc("C.E");
        }
 
        String resp =  uservice.insertarPersona(persona);
        //mav.addObject("personasale", persona);
        mav.addObject("respuesta", resp);
        return mav;
    }
    
    
    //--------------------------------------------------------------------------------------
    
    @RequestMapping(params = "home")
    public ModelAndView nosotros(){
        ModelAndView mav = new ModelAndView("nosotros");
        
        return mav;
        
    }
    //-----------------------------------------------------------------------------------------
    //login
    
    @RequestMapping(value = "login", method=RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout){
      ModelAndView mav = new ModelAndView();
     
       if (error != null) {
		mav.addObject("error", "¡Usuario o password invalido!");
	  }

	  if (logout != null) {
		mav.addObject("msg", "¡Has cerrado sesión exitosamente! ");
	  }
	 mav.setViewName("login");
     return mav;
    }
    
    //-----------------------------------------------------------------------------------------
    //for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {
             ModelAndView model = new ModelAndView();
           /** Principal user
            * if (user != null) {
             model.addObject("msg", "Hi " + user.getName()
             + ", You can not access this page!");
            } else {
             model.addObject("msg",
             "You can not access this page!");
            }

            model.setViewName("403");
            return model;
	 ModelAndView model = new ModelAndView();**/
		
	  //check if user is login
	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  if (!(auth instanceof AnonymousAuthenticationToken)) {
		UserDetails userDetail = (UserDetails) auth.getPrincipal();	
		model.addObject("username", userDetail.getUsername());
	  }
		
	  model.setViewName("403");
	  return model; 

	}
}

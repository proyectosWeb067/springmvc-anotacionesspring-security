
package com.empresa.proyecto.dto;

 


public class PersonaDTO {
    
    private int idusuario;
    private String nombre;
    private String apellPat;
    private String apellMat;
    private String  tipodoc;
    private int numdoc;
    private int dianac;
    private String mesnac;
    private int anac;
    private String sexo;
    private int ruc;
    private String estadcivil;
    private String estadolabo;

    public PersonaDTO() {
    }

    public int getIdusuario(){
      return idusuario;
    }
    
    public void setIdusuario(int idusuario){
        this.idusuario = idusuario;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellPat() {
        return apellPat;
    }

    public void setApellPat(String apellPat) {
        this.apellPat = apellPat;
    }

    public String getApellMat() {
        return apellMat;
    }

    public void setApellMat(String apellMat) {
        this.apellMat = apellMat;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public int getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(int numdoc) {
        this.numdoc = numdoc;
    }

    public int getDianac() {
        return dianac;
    }

    public void setDianac(int dianac) {
        this.dianac = dianac;
    }

    public String getMesnac() {
        return mesnac;
    }

    public void setMesnac(String mesnac) {
        this.mesnac = mesnac;
    }

    public int getAnac() {
        return anac;
    }

    public void setAnac(int anac) {
        this.anac = anac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getRuc() {
        return ruc;
    }

    public void setRuc(int ruc) {
        this.ruc = ruc;
    }

    public String getEstadcivil() {
        return estadcivil;
    }

    public void setEstadcivil(String estadcivil) {
        this.estadcivil = estadcivil;
    }

    public String getEstadolabo() {
        return estadolabo;
    }

    public void setEstadolabo(String estadolabo) {
        this.estadolabo = estadolabo;
    }

    
    
}

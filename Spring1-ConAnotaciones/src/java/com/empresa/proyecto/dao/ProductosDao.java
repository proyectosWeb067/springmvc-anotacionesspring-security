 
package com.empresa.proyecto.dao;
 
import com.empresa.proyecto.dto.ProductoDTO;
import java.util.List;

 
public interface ProductosDao {
    
   public List<ProductoDTO> getAll();
   public String insertarProducto(ProductoDTO p);
   public String eliminarProducto(List<Object[]> ids);
   public String actualizarProducto(ProductoDTO p);
   public ProductoDTO getProducto(Integer idproducto);
   
    
}

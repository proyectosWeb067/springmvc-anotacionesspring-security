 
package com.empresa.proyecto.service;

import com.empresa.proyecto.dto.PersonaDTO;
import java.util.List;

 
public interface UsuarioService {
    
    public List<PersonaDTO> getAll();
   public String insertarPersona(PersonaDTO p);
   public String eliminarPersona(List<Object[]> ids);
   public String actualizarPersona(PersonaDTO p);
   public PersonaDTO getPersona(Integer idpersona);
}

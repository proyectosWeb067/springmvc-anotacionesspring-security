  
package com.empresa.proyecto.service;

import com.empresa.proyecto.dto.ProductoDTO;
import java.util.List;

 
public interface ProductoService {
    
    public List<ProductoDTO> getAll();
   public String insertarProducto(ProductoDTO p);
   public String eliminarProducto(List<Object[]> ids);
   public String actualizarProducto(ProductoDTO p);
   public ProductoDTO getProducto(Integer idproducto);
}

 
package com.empresa.proyecto.serviceimpl;

import com.empresa.proyecto.dao.UsuarioDao;
import com.empresa.proyecto.dto.PersonaDTO;
import com.empresa.proyecto.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService{

    
    //inyeccion de dependencias. con anotacion autowired
    @Autowired
    private UsuarioDao udao;
    
 
    @Override
    public List<PersonaDTO> getAll() {
         List<PersonaDTO> l = udao.getAll();
           
         return l;
    }

    @Override
    public String insertarPersona(PersonaDTO p) {
         String resp = udao.insertarPersona(p);
          
         return resp;
    }

    @Override
    public String eliminarPersona(List<Object[]> ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String actualizarPersona(PersonaDTO p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PersonaDTO getPersona(Integer idpersona) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}

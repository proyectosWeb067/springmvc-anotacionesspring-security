 
package com.empresa.proyecto.daoImplement;

import com.empresa.proyecto.dao.ProductosDao;
import com.empresa.proyecto.dto.ProductoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


@Repository//se agrega en dao para cuando alguien solicite entregue la implementacion y al que la solicita se agrega la inyeccion de dependencia 
public class ProductosDaoImplement implements ProductosDao{

    //IoC (Patron Inversion de control, tecnica inyecccion de dependencias) con anotacion autowired
    
    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    } 
   
    @Override
    public List<ProductoDTO> getAll() {
        String sql = "SELECT "+"idproducto,"+"titulo,"+"tipo,"+"precio,"+"descripcion "+" FROM productos"+" ORDER BY titulo";
        RowMapper mapper = new RowMapper() {
            @Override
            public Object mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
               
                ProductoDTO p = new ProductoDTO();
                p.setIdproducto(rs.getInt(1));
                p.setTitulo(rs.getString(2));
                p.setTipo(rs.getString(3));
                p.setPrecio(rs.getDouble(4));
                p.setDescripcion(rs.getString(5));
                
                return p;
            }
        };
         List<ProductoDTO> listap =  jdbcTemplate.query(sql, mapper);
        return listap;
               
    }

    @Override
    public String insertarProducto(ProductoDTO p) {
        String resp = "";
        String sql = "INSERT INTO productos(titulo,tipo,precio,descripcion)"+ "VALUES(?,?,?,?)";
        
         int re = jdbcTemplate.update(sql,
                p.getTitulo(),
                p.getTipo(),
                p.getPrecio(),
                p.getDescripcion());
         if(re == 0  ){
             resp ="0 filas afectadas";
         }else{
             resp="Gracias,Producto registrado";
         }
         
        return resp;
    }

    @Override
    public String eliminarProducto(List<Object[]> ids) {
        String res = null;
         String sql = "DELETE FROM productos WHERE idproducto = ?";
         
         int[] resp = jdbcTemplate.batchUpdate(sql, ids);
         
         if(resp.length != ids.size()){
             res = "Algunas filas no retiradas";
         }else{
             res = "El producto se elimino.";
         }
         
         return res;
    }

    @Override
    public String actualizarProducto(ProductoDTO p) {
        String res = null;
        String sql = "UPDATE productos SET "+" titulo=?, tipo=?, precio=?, descripcion=? " +" WHERE idproducto= ?";
        int resp = jdbcTemplate.update(sql, p.getTitulo(), p.getTipo(), p.getPrecio(), p.getDescripcion());
        
        if(resp == 0){
            res = "No se pudo realizar su solicitud";
        }
        else{
            res= "Su producto se elimino satisfactoriamente";
        }
        
        return res;
    }

    @Override
    public ProductoDTO getProducto(Integer idproducto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}

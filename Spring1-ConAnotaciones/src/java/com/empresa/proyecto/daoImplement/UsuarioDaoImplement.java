
package com.empresa.proyecto.daoImplement;
 
import com.empresa.proyecto.dao.UsuarioDao;
import com.empresa.proyecto.dto.PersonaDTO;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


@Repository
public class UsuarioDaoImplement implements UsuarioDao {
    
    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
     public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    } 

    
    @Override
    public List<PersonaDTO> getAll() {
        String sql = "SELECT "+"idusuario,"+"nombre,"+"apellpat,"+"apellmat,"+"numdoc,"+"estadolabo "+" FROM usuario"+" ORDER BY nombre";
        RowMapper mapper = new RowMapper() {
            @Override
            public Object mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
          
                PersonaDTO p = new PersonaDTO();
                p.setIdusuario(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setApellPat(rs.getString(3));
                p.setApellMat(rs.getString(4));
                p.setNumdoc(rs.getInt(5));
                p.setEstadolabo(rs.getString(6));
                return p;
            }
        };
         List<PersonaDTO> listap =  jdbcTemplate.query(sql, mapper);
          
        return listap;
               
     }

    @Override
    public String insertarPersona(PersonaDTO p) {
        String resp = "";
        String sql = "INSERT INTO usuario(nombre,apellpat,apellmat,tipodoc,numdoc,dianac,mesnac,anac,sexo,ruc,estadcivil,estadolabo)"+"VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        int respsql = jdbcTemplate.update(sql,
                p.getNombre(),
                p.getApellPat(),
                p.getApellMat(),
                p.getTipodoc(),
                p.getNumdoc(),
                p.getDianac(),
                p.getMesnac(),
                p.getAnac(),
                p.getSexo(),
                p.getRuc(),
                p.getEstadcivil(),
                p.getEstadolabo());
        if(respsql == 0){
            resp = " 0 filas afectadas, por favor comuniquese con un asesor cercano";
        }else{
            resp = "Gracias, Su solicitud fue en enviada";
        }
        
        
        return resp;
    }

    @Override
    public String eliminarPersona(List<Object[]> ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String actualizarPersona(PersonaDTO p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PersonaDTO getPersona(Integer idpersona) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
